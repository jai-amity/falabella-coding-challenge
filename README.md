# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Falabella Coding Challenge
* Version v1.0
* Author : Jai Singh

### How do I get set up? ###
### ---- Using Docker ---- ###

* Summary of set up Docker // Written by Jai Singh
* Prequisites: Docker and Docker-compose should be installed [Optional, require if want to run code with Docker]
* Dependencies: phpunit, PHP7, PSR4 (autoload) using composer

* How to setup

Run command "docker-compose up -d" 
		--- OR ---
Run command "docker build -t number-app ." AND "docker run -p 8000:80 number-app -d"

* How to run code

Run "http://localhost:8000" on browser


### How do I get set up? ###
### ---- W/O Docker ---- ###

* Summary of set up code // Written by Jai Singh
* Prequisites: composer, Apache2, PHP7 
* Dependencies: phpunit, PHP7, PSR4 (autoload) using composer

* How to run code

Run command "composer update" 

* How to run code

Run "http://localhost/<code_location>" on browser

