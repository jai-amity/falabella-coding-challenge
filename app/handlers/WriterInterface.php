<?php
/**
 * Created by Jai Singh.
 */

namespace app\handlers;


interface WriterInterface
{
    /**
     * @return Falabella Name
     */
    public function getName();

}
