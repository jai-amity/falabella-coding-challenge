<?php
/** 
  * Number3Writer is a class for number printing.
  *
  * @author Jai Singh
  * @access public 
  */

namespace app\handlers;

class Number3Writer implements \app\handlers\WriterInterface
{
    /** 
     *  Returns the String 
     * 
     *  @return array all of the exciting sample options 
     *  @access public 
    */
    public function getName(){
        return 'Linio';
    }

}
