<?php
/** 
  * Modulo is a class for number printing.
  *
  * @author Jai Singh
  * @access public 
  */

namespace app\services;

class Modulo
{
     /**
     * numeric value to manupulate
     *
     * @var number
     */
    public $number;
    
    /** 
     *  Constructer to initialize variable
     * 
    */
    public function __construct($num)
    {
        $this->number = $num;
    }
    
    /** 
     *  Returns the modulo reminder
     * 
     *  @param $divisor numeric value
     *  @return number modulo remiber 
     *  @access public 
    */
    public function getRemainder($divisor) {
        return $this->number % $divisor;
    }

    /** 
     *  Writer string 
     * 
     *  @param class WriterInterface
     *  @access public 
    */
    public function write(\app\handlers\WriterInterface $writer) {
        echo sprintf("Number %d ------> <b>%s</b></br>", $this->number, $writer->getName());
    }

}
