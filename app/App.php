<?php

 /** 
  * App is a class for number printing.
  *
  * @author Jai Singh
  * @access public 
  */

namespace app;

use app\services\Modulo;
use app\handlers;

class App {

    /**
     * numeric value to manipulate
     *
     * @var number $range_start
     */
    private $range_start;

    /**
     * numeric value to manupulate
     *
     * * @var number $range_end
     */
    private $range_end;

    /** 
     *  Constructer to initialize variable
     * 
     * @param array $range
    */
    public function __construct(array $range) {
        
        $this->range_start = array_slice($range, 0, 2)[0];
        $this->range_end = array_slice($range, 0, 2)[1];
    }

    /** 
     *  Write the conversion of number
     * 
     *  @access public 
    */
    public function run() {

        $head = array_reduce(range($this->range_start, $this->range_end), function ($head, $number) {
            try {
                $modulo_obj = new Modulo($number);
                switch ($modulo_obj) {
                    case $modulo_obj->getRemainder(3) == 0 && $modulo_obj->getRemainder(5) == 0:
                        $head = new \app\handlers\Number5And3Writer();
                        break;
                    case $modulo_obj->getRemainder(3) == 0:
                        $head = new \app\handlers\Number3Writer();
                        break;
                    case $modulo_obj->getRemainder(5) == 0:
                        $head = new \app\handlers\Number5Writer();
                        break;
                    default:
                        throw new \Exception('unsupported number');
                }
                $modulo_obj->write($head);
                
            } catch (\Exception $ex) {
                echo sprintf("Number %d ------> <b>%s</b></br>", $number, $number);
            }
        });
        
    }

    /** 
     *  Returns the modulo reminder
     * 
     *  @param $divisor numeric value
     *  @return number modulo remiber 
     *  @access public 
    */
    public function runSecoundApproach(array $range) {

        $range_start = array_slice($range, 0, 2)[0];
        $range_end = array_slice($range, 0, 2)[1];
        $head = array_reduce(range($range_start, $range_end), function ($head, $number) {
            try {
                $modulo_obj = new Modulo($number);
                $modulo_obj->getRemainder(3) == 0 && $head = new \app\handlers\Number3Writer();
                $modulo_obj->getRemainder(5) == 0 && $head = new \app\handlers\Number5Writer();
                $modulo_obj->getRemainder(3) == 0 && $modulo_obj->getRemainder(5) == 0 && $head = new \app\handlers\Number5And3Writer();
              
              if ($head)
              $modulo_obj->write($head);
                
            } catch (\Exception $ex) {
                echo sprintf("Number %d ------> <b>%s</b></br>", $number, $number);
            }
        });
    }

}
