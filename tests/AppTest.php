<?php
/** 
  * AppTest is a class for PHPUnit test
  *
  * @author Jai Singh
  * @access public 
  */

namespace tests;
use PHPUnit\Framework\TestCase;


class AppTest extends TestCase
{

    public function testThree() {
        $modulotst = new \app\services\Modulo(3);    
        $this->assertEquals(0, $modulotst->getRemainder(3));
    }

    public function  testFive(){
        $modulotst = new \app\services\Modulo(10);
        $this->assertEquals(0, $modulotst->getRemainder(5));
    }

    public function  testThreeAndFive(){

        $modulotst = new \app\services\Modulo(15);
        $this->assertEquals(0, $modulotst->getRemainder(3));
        $this->assertEquals(0, $modulotst->getRemainder(5));
    }
    
    public function testWriteThree() {
        $modulotst = new \app\services\Modulo(3);  
        $writer = new \app\handlers\Number3Writer;
        $this->expectOutputString(sprintf("Number %d ------> <b>%s</b></br>", $modulotst->number, 'Linio'), $modulotst->write($writer));
    }
    
    public function testWriteFive() {
        $modulotst = new \app\services\Modulo(10);  
        $writer = new \app\handlers\Number5Writer;
        $this->expectOutputString(sprintf("Number %d ------> <b>%s</b></br>", $modulotst->number, 'IT'), $modulotst->write($writer));
    }   
    
    public function testWriteThreeAndFive() {
        $modulotst = new \app\services\Modulo(15);  
        $writer = new \app\handlers\Number5And3Writer;
        $this->expectOutputString(sprintf("Number %d ------> <b>%s</b></br>", $modulotst->number, 'Linianos'), $modulotst->write($writer));
    }

}
