FROM php:7.2-apache

RUN apt-get update && \
	    apt-get install -y --no-install-recommends git zip

RUN curl -sS https://getcomposer.org/installer | php \
              && mv composer.phar /usr/local/bin/ \
	              && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

COPY ./ /var/www/html/
WORKDIR /var/www/html

RUN composer install
#ENV PATH="~/.composer/vendor/bin:./vendor/bin:${PATH}"
