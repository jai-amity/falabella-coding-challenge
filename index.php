<?php

 /** 
  * Number challange code
  *
  * Can be run with or without Docker
  * @author Jai Singh
  */

require 'vendor/autoload.php';

use app\App;
use tests\AppTest;

$obj = new App([1,100]);

// FIRST APPROACH
$obj->run();

// FIRST APPROACH
//$obj->runSecoundApproach([1,100]);

// UNIT TESTs
//$test = new AppTest();
